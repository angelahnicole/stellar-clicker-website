# Stellar Clicker Website

![Untitled-1.jpg](https://bitbucket.org/repo/kLL6Gq/images/1397576366-Untitled-1.jpg)

* [SC Website](http://stellar.polymorphixgaming.com)
* [SC Game Source Code](https://bitbucket.org/angelahnicole/stellar-clicker-game)

## REQUIREMENTS

### Non-Functional
* The home page, blog, and wrapper page for the wiki is powered by the PHP framework Laravel 5.2
* The wiki itself is powered by wikia
* The forum is a self-contained application that is powered by beta software called Flarum
* The version of PHP used is PHP 5.5.33
* The Laravel MVC framework uses jQuery, Less, and Bootstrap front-end technologies
* The mail server used to send out forgotten password emails is mailgun
* The database used is MariaDB version 5.5.44
* The server is hosted on CentOS 7

### Functional
* Users can register for accounts
* Separate accounts for forum, blog, and wiki (unfortunately)
* Users can update their accounts
* Users can post to the forum
* Registered forum users can post to the forum
* Bloggers can create, delete, or edit their blog submissions
* Admins can create, update, and delete blog submissions
* Admins can create, update, and delete users
* Admins can update user permissions
* Ability to navigate to wiki, forum, blog, and homepage

## WEBSITE SCREENS

![Untitled-2.jpg](https://bitbucket.org/repo/kLL6Gq/images/2922698360-Untitled-2.jpg)

![Untitled-7.jpg](https://bitbucket.org/repo/kLL6Gq/images/701484116-Untitled-7.jpg)

![Untitled-4.jpg](https://bitbucket.org/repo/kLL6Gq/images/2846222077-Untitled-4.jpg)

![Untitled-10.jpg](https://bitbucket.org/repo/kLL6Gq/images/3924836513-Untitled-10.jpg)

![Untitled-12.jpg](https://bitbucket.org/repo/kLL6Gq/images/4290654357-Untitled-12.jpg)

![Untitled-13.jpg](https://bitbucket.org/repo/kLL6Gq/images/2940037224-Untitled-13.jpg)

![Untitled-14.jpg](https://bitbucket.org/repo/kLL6Gq/images/418689816-Untitled-14.jpg)

![Untitled-16.jpg](https://bitbucket.org/repo/kLL6Gq/images/4116334544-Untitled-16.jpg)

![Untitled-22.jpg](https://bitbucket.org/repo/kLL6Gq/images/1062464584-Untitled-22.jpg)

![Untitled-23.jpg](https://bitbucket.org/repo/kLL6Gq/images/1561824613-Untitled-23.jpg)

![Untitled-25.jpg](https://bitbucket.org/repo/kLL6Gq/images/2143008867-Untitled-25.jpg)

![Untitled-26.jpg](https://bitbucket.org/repo/kLL6Gq/images/2899206678-Untitled-26.jpg)

![Untitled-28.jpg](https://bitbucket.org/repo/kLL6Gq/images/17471738-Untitled-28.jpg)